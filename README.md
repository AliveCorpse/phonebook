#Simple Phone Book for test.#

### Перед запуском приложения необходимо ###
* Импортировать 'phonebook.sql' из корневой директории проекта для создания базы данных и необходимых таблиц
* Настроить соединение с базой данных можно в /App/configs/db.cfg
* Установить все зависимости при помощи **Composer** ('*composer install*')
* Внешней корневой папкой является 'web'


На момент отправки задания недоработана система отображения ошибок, пароли не хешируюься и хранятся в открытом виде...