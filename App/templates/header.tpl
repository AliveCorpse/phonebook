<html>
    <head>
        <title>
            Phone Book
        </title>
        <link href="../vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
    <div class="errors">
    {$error|default: ''}

    {php}
       if(!empty($_SESSION['error'])){
            echo $_SESSION['error'];
            unset($_SESSION['error']);
         }
    {/php}
    </div>
    <div class="container-fluid">
    <div class="col-md-8 col-md-offset-2">