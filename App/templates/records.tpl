<div class="row">
<div class="col-md-12">
<table class="table table-bordered table-condensed">

    <thead>
        <tr>
            <th>First Name</th>
            <th>Second Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Addres</th>
            <th>Photo</th>
            <th>Action</th>
        </tr>
    </thead>

    <tbody class="table-striped">
    {if !empty($records)}
        {foreach $records as $record}
        <tr>    
            <td>{$record->first_name}</td>     
            <td>{$record->second_name}</td>     
            <td><a href="mailto:{$record->email}">{$record->email}</a></td>     
            <td><a href="tel:{$record->phone}">{$record->phone}</a></td>     
            <td>{$record->address}</td>     
            <td><img class="img-rounded img-responsive center-block" src="{$record->photo}" /></td>
            <td>
                <button class="edit" value="{$record->id}">Edit</button>
                <button class="delete" value="{$record->id}">Delete</button>
            </td>     
        </tr>
        {/foreach}
    {/if}
    </tbody>

</table>
</div>
</div>