<div class="row">
    <div class="col-md-6">
    <form class="form-horizontal" action="index.php" method="post" id="editrecord">
        <input type="hidden" name="id" value="{$record->id|default: ''}">

        <div class="form-group">
            <label class="col-md-4 control-label" for="firstname">First Name</label>
            <div class="col-md-8">
                <input class="form-control" id="firstname" type="text" name="fname" value="{$record->first_name|default: ''}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="secondname">Second Name</label>
            <div class="col-md-8">
                <input class="form-control" id="secondname" type="text" name="sname" value="{$record->second_name|default: ''}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-8">
                <input class="form-control" id="email" type="email" name="email" value="{$record->email|default: ''}">
                </div>
            <span class="col-md-4 help-block" id='email'></span>
            
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="phone">Phone</label>
            <div class="col-md-8">
                <input class="form-control" id="phone" type="text" name="phone" value="{$record->phone|default: ''}">
                </div>
            <span class="col-md-4 help-block" id='phone'></span>
            
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="address">Address</label>
            <div class="col-md-8">
                <input class="form-control" id="address" type="text" name="address" value="{$record->address|default: ''}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="photo">Photo</label>
            <div class="col-md-8">
                <input id="photo" type="file" name="photo">
            </div>
            
        </div>
        <input class="form-control btn btn-primary" type="submit" name="save" value="Save">
    </form>
    </div>
    <div class="col-md-6">
        <img class="img-rounded img-responsive center-block" src="{$record->photo|default: ''}" id='output'>
    </div>
</div>