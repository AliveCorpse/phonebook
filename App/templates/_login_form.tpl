<div class="row">
    <div class="col-md-5">
        <h2>Login</h2>
        <form class="form-horizontal" action="index.php" method="post" id="loginform">
            <div class="form-group">
                <label class="control-label" for="loginname">Name</label>
                <input class="form-control" name="loginname" type="text" id="loginname" required \>
            </div>
            <div class="form-group">
                <label class="control-label" for="loginpassword">Password</label>
                <input class="form-control" name="loginpassword" type="password" id="loginpassword" required>
                <span class="help-block"></span>
            </div> 
            <input class="btn btn-success form-control" type="submit" value="Login">  
        </form>
    </div>

    <div class="col-md-5 col-md-offset-1">
        <h2>Registration</h2>
        <form class="form-horizontal" action="index.php" method="post" id="registrationform">
            <div class="form-group">
                <label class="control-label" for="regname">Name</label>
                <input class="form-control" name="regname" type="text" id="regname" required>
            </div>
            <div class="form-group">
                <label class="control-label" for="regpassword">Password</label>
                <input class="form-control" name="regpassword" type="password" id="regpassword" required>
            <span class="help-block"></span>
            </div>
            <input class="btn btn-warning form-control" type="submit" value="Registrate">  
        </form>
    </div>
</div>