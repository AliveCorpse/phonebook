<?php

namespace App\Core;

class Db
{
    use Singleton;
    
    private $dbh;

    protected function __construct()
    {
        $ini = Config::instance();
//        $this->dbh = new \PDO('mysql:host=127.0.0.1;dbname=course2',
//                'mysql', 'mysql');
        try{
            $this->dbh = new \PDO($ini->config['db']
                    . ':host=' . $ini->config['host']
                    . ';dbname=' . $ini->config['dbname'],
                    $ini->config['login'],
                    $ini->config['password']
                    );
        } catch (\PDOException $e) {
           throw new Exceptions\Db($e->getMessage());
        }
    }

    public function query($sql, $params = [], $class='')
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);

        if(empty($class)){
            return $sth->fetchAll();
        } else {
            return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        }
        return [];
    }

    public function execute($sql, $params = [])
    {
        $sth = $this->dbh->prepare($sql);
        return $sth->execute($params);
    }
    
    public function insertId() 
    {
        return $this->dbh->lastInsertId();
    }
}