<?php

namespace App\Core;

class Config
{
    use Singleton;
    
    public $config;
    
    protected function __construct(){
       $this->config = parse_ini_file(__DIR__ . '/../configs/db.cfg');
    }
    
}
