<?php

namespace App\Core;

abstract class Model
{

    const TABLE = '';

    public $id;

    public static function findAll()
    {
        $db = Db::instance();
        return $db->query(
            'SELECT * FROM ' . static::TABLE,
            [],
            static::class
        );
    }

    public static function findById(int $id)
    {
        $db  = Db::instance();
        $res = $db->query(
            'SELECT * FROM ' . static::TABLE .
            ' WHERE id=:id',
            [':id' => $id],
            static::class
        );

        if (!empty($res)) {
            return $res[0];
        }
        return false;
    }

    public static function findByName($name)
    {
        $db  = Db::instance();
        $res = $db->query(
            'SELECT * FROM ' . static::TABLE .
            ' WHERE name=:name',
            [':name' => $name],
            static::class
        );

        if (!empty($res)) {
            return $res[0];
        }
        return false;
    }
    public static function findByUserId($id)
    {
        $db  = Db::instance();
        $res = $db->query(
            'SELECT * FROM ' . static::TABLE .
            ' WHERE user_id=:user_id',
            [':user_id' => $id],
            static::class
        );

        if (!empty($res)) {
            return $res;
        }
        return false;
    }

    protected function isNew()
    {
        return empty($this->id);
    }

    protected function insert()
    {
        $columns = [];
        $values  = [];
        foreach ($this as $k => $v) {
            if ('id' == $k) {
                continue;
            }
            $columns[]        = $k;
            $values[':' . $k] = $v;
        }
        $sql = 'INSERT INTO ' . static::TABLE .
        ' (' . implode(',', $columns) . ') '
        . 'VALUES (' . implode(',', array_keys($values)) . ')';

        $db = Db::instance();
        $db->execute($sql, $values);

        $this->id = $db->insertId();
    }

    protected function update()
    {
        $values    = [];
        $setString = [];
        foreach ($this as $k => $v) {
            if ('id' == $k) {
                continue;
            }
            $values[':' . $k] = $v;
            $setString[]      = $k . '=:' . $k;
        }

        $sql = 'UPDATE ' . static::TABLE .
        ' SET ' . implode(',', $setString)
            . ' WHERE id=:id';
        $values[':id'] = $this->id;

        $db = Db::instance();
        $db->execute($sql, $values);
    }

    public function save()
    {
        if ($this->isNew()) {
            $this->insert();
        } else {
            $this->update();
        }
    }

    public function delete()
    {
        $sql = 'DELETE FROM ' . static::TABLE
            . ' WHERE id=:id';
        $db = Db::instance();
        $db->execute($sql, [':id' => $this->id]);
    }
}
