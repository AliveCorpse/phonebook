<?php

namespace App\Core;

class View implements \Countable
{
    protected $data = [];

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        return $this->data[$name];
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function display($template)
    {
        echo $this->render($template);
    }

    public function render($template)
    {
        ob_start();

        foreach ($this->data as $name => $value) {
            $$name = $value;
        }
        include $template;
        $content = ob_get_contents();

        ob_end_clean();

        return $content;
    }

    public function count()
    {
        return count($this->data);
    }
}
