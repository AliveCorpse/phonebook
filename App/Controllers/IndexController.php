<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\Record;
use App\Models\User;

class IndexController extends Controller
{
    protected $smarty;

    public function __construct($smarty)
    {
        $this->smarty = $smarty;
    }

    protected function actionIndex()
    {
        $this->sendHtml();
    }

    protected function actionRegistration()
    {
        if (!empty(User::findByName(
            filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING)
        ))) {
            $_SESSION['error'] = 'User already exist!';
            $this->smarty->assign('error', 'User already exist!');
            $this->sendHtml();
            return;
        }
        if (!empty($_POST['name']) && !empty($_POST['password'])) {

            $user = new User();

            $user->name     = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $user->password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
            $user->save();
            $user->login();

            $this->sendHtml();
        }
    }


function actionLogin()
{
    $user = User::findByName(filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING));

    if (!empty($user) && filter_input(INPUT_POST, 'password') === $user->password) {
        $user->login();

        $this->sendHtml();

    } else {
        $_SESSION['error'] = 'Wrond login or password';

        $this->sendHtml();
    }

}

function actionLogout()
{
    $user = User::findById(User::getUser());

    if (!User::isGuset()) {
        User::logout();

        $this->sendHtml();

    }
}

function actionAddrecord()
{
    $record     = new Record();
    $record->id = (!empty($_POST['id']))
    ? filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT)
    : null;
    $record->user_id     = User::getUser();
    $record->first_name  = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
    $record->second_name = filter_input(INPUT_POST, 'sname', FILTER_SANITIZE_STRING);
    $record->email       = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $record->phone       = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_NUMBER_INT);
    $record->address     = filter_input(INPUT_POST, 'address', FILTER_SANITIZE_STRING);

    if (UPLOAD_ERR_NO_FILE === $_FILES['photo']['error']
        && !empty($_POST['id'])) {
        $record->photo = Record::findById($_POST['id'])->photo;
    } else {
        $record->photo = $this->uploadFile('photo');
    }

    $record->save();

    $this->sendHtml();
}

function actionDelrecord()
{
    $record = Record::findById($_GET['id']);
    if ($record) {
        $record->delete();

        $this->sendHtml();
    } else {
        $_SESSION['error'] = 'This record does not exist!';
    }
}

function actionEditrecord()
{
    $record = Record::findById($_GET['id']);
    if ($record) {
        $main_content = $this->smarty->fetch('header.tpl');
        $main_content .= '<button id="logout">Logout</button><hr>';
        $this->smarty->assign('record', $record);
        $main_content .= $this->smarty->fetch('_record_form.tpl');
        $records = Record::findByUserId(User::getUser());
        $this->smarty->assign('records', $records);
        $main_content .= $this->smarty->fetch('records.tpl');
        $main_content .= $this->smarty->fetch('footer.tpl');

        echo $main_content;
    } else {
        $_SESSION['error'] = 'This record does not exist!';
    }

}

function uploadFile($field)
{
    try {

        // Undefined | Multiple Files | $_FILES Corruption Attack
        // If this request falls under any of them, treat it invalid.
        if (
            !isset($_FILES[$field]['error']) ||
            is_array($_FILES[$field]['error'])
        ) {
            throw new \RuntimeException('Invalid parameters.');
        }

        // Check $_FILES[$field]['error'] value.
        switch ($_FILES[$field]['error']) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new \RuntimeException('No file sent.');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new \RuntimeException('Exceeded filesize limit.');
            default:
                throw new \RuntimeException('Unknown errors.');
        }

        // You should also check filesize here.
        if ($_FILES[$field]['size'] > 1000000) {
            throw new \RuntimeException('Exceeded filesize limit.');
        }

        // DO NOT TRUST $_FILES[$field]['mime'] VALUE !!
        // Check MIME Type by yourself.
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        if (false === $ext = array_search(
            $finfo->file($_FILES[$field]['tmp_name']),
            array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
            ),
            true
        )) {
            throw new \RuntimeException('Invalid file format.');
        }

        // You should name it uniquely.
        // DO NOT USE $_FILES[$field]['name'] WITHOUT ANY VALIDATION !!
        // On this example, obtain safe unique name from its binary data.
        $file = sprintf('/images/%s.%s', sha1_file($_FILES[$field]['tmp_name']), $ext);

        if (!move_uploaded_file(
            $_FILES[$field]['tmp_name'],
            __DIR__ . '/../../' . $file
        )) {
            throw new \RuntimeException('Failed to move uploaded file.');
        }

        return $file;
        // echo 'File is uploaded successfully.';

    } catch (\RuntimeException $e) {

        echo $e->getMessage();

    }
}

function sendHtml()
{
    $main_content = '';

    if (User::isGuset()) {
        $main_content = $this->smarty->fetch('header.tpl');
        $main_content .= $this->smarty->fetch('_login_form.tpl');
        $users = User::findAll();
        $this->smarty->assign('users', $users);
        $main_content .= $this->smarty->fetch('index.tpl');
        $main_content .= $this->smarty->fetch('footer.tpl');
    } else {

        $main_content = $this->smarty->fetch('header.tpl');
        $main_content .= '
                <div class="row">
                    <div class="col-md-3 col-md-offset-4">
                        <button class="btn btn-danger form-control" id="logout">Logout</button>
                    </div>
                </div><hr>';
        $main_content .= $this->smarty->fetch('_record_form.tpl');
        $records = Record::findByUserId(User::getUser());
        $this->smarty->assign('records', $records);
        $main_content .= $this->smarty->fetch('records.tpl');
        $main_content .= $this->smarty->fetch('footer.tpl');
    }

    echo $main_content;
}

}
