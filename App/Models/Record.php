<?php

namespace App\Models;

use App\Core\Model;

class Record extends Model
{
    const TABLE = 'records';

    public $id;
    public $user_id;
    public $first_name;
    public $second_name;
    public $email;
    public $phone;
    public $address;
    public $photo;

    

}
