<?php

namespace App\Models;

use App\Core\Model;

class User extends Model
{
    const TABLE = 'users';

    public $id;
    public $name;
    public $password;

    public function login()
    {
        setcookie('auth', $this->id, time() + 86400);
    }

    public static function logout()
    {
        setcookie('auth', '', time() - 86400);
    }

    public static function isGuset()
    {
        return !isset($_COOKIE['auth']);
    }

    public static function getUser()
    {
        return empty($_COOKIE['auth']) ? false : $_COOKIE['auth'];
    }

}
