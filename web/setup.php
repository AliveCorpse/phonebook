<?php

// load Smarty library
require_once __DIR__ . '/../vendor/autoload.php';

spl_autoload_register(function ($class) {
    require __DIR__ . '\..\\' . str_replace('\\', '/', $class) . '.php';
});

class SmartyPhoneBook extends SmartyBC
{

    public function __construct()
    {

// Class Constructor.
        // These automatically get set with each new instance.

        parent::__construct();

        $this->setTemplateDir(__DIR__ . '/../app/templates/');
        $this->setCompileDir(__DIR__ . '/../app/templates_c/');
        $this->setConfigDir(__DIR__ . '/../app/configs/');
        $this->setCacheDir(__DIR__ . '/../app/cache/');

        $this->caching = Smarty::CACHING_LIFETIME_CURRENT;
        $this->assign('app_name', 'Phone Book');
    }

}
