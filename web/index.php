<?php
session_start();
require __DIR__ . '/setup.php';

// $DB = new DbSimple\Connect("mysql://mysql:mysql@localhost/phonebook");

$smarty = new SmartyPhoneBook();
$smarty->debugging = false;
$smarty->caching = false;

$controller = new App\Controllers\IndexController($smarty);
$action = (filter_input(INPUT_GET, 'action')) ?: 'index';
$controller->action($action);