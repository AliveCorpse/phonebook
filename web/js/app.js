$(document).ready(function(){

    $('form#loginform').submit(function(event){
        event.preventDefault();

        $.ajax({
            method: "POST",
            async: true,
            url: "index.php?action=login",
            data: {
                name: $("form#loginform input[name='loginname']").val(),
                password: $("form#loginform input[name='loginpassword']").val()
            },
            success: function(result){
                $.get('index.php', function(data){
                    $('body').html(data);
                });
            }
        });
    });

    $("button#logout").click(function(){
        $.ajax({
            method: "POST",
            async: true,
            url: "index.php?action=logout",
            success: function(result){
                $.get('index.php', function(data){
                    $('body').html(data);
                });
            }
        });
    });

    $("button.delete").click(function(){
        $.ajax({
            method: "GET",
            url: "index.php?action=delrecord",
            data: {id: this.value},
            success: function(result){
                $("body").html(result);
            }
        });
    });

    $("button.edit").click(function(){
        $.ajax({
            method: "GET",
            url: "index.php?action=editrecord",
            data: {id: this.value},
            success: function(result){
                $("body").html(result);
            }
        });
    });

    $("form#editrecord").submit(function(event){
        event.preventDefault();

        var email_pattern = /^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/igm;
        var email_field = $("form#editrecord input[name='email']" ).val();

        var phone_pattern = /^(\d|\+)\d{0,12}$/igm;
        var phone_field = $("form#editrecord input[name='phone']" ).val();

        if ((false != email_pattern.test(email_field))
            && (false != phone_pattern.test(phone_field))) {
            $.ajax({
                method: "POST",
                url: "index.php?action=addrecord",
                cache: false,
                processData: false,
                contentType: false,
                data: new FormData(this),
                success: function(result){
                    $("body").html(result);
                }
            });
        }

        if(false == email_pattern.test(email_field))
        {
            $("form#editrecord span#email")
                .text( "Некорректный формат Email`a!" )
                .show().fadeOut( 2000 );
            event.preventDefault();
        }

        if(false == phone_pattern.test(phone_field))
        {
            $("form#editrecord span#phone")
                .text( "Некорректный формат номера телефона" )
                .show().fadeOut( 2000 );
            event.preventDefault();
        }
    });

    $('form#registrationform').submit(function(event){
        event.preventDefault();

        var password_pattern = /^[a-z0-9_-]{8,20}$/igm;
        var password_field = $("form#registrationform input[name='regpassword']" ).val();

        if (false != password_pattern.test(password_field)) {
            $.ajax({
            method: "POST",
            url: "index.php?action=registration",
            data: {
                name: $("form#registrationform input[name='regname']").val(),
                password: $("form#registrationform input[name='regpassword']").val()
            },
            success: function(result){
                $.get('index.php', function(data){
                        $('body').html(data);
                    });
                }
            });
        }else{
            $( "form#registrationform span" )
            .text( "Пароль должен быть минимум 8 символов и состоять из букв и цифр" )
            .show().fadeOut( 2000 );
            event.preventDefault();
        }
        
    });
// Preview
    $("form#editrecord input:file").change(function(event) {
        var input = event.target;

        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var output = document.getElementById('output');
            output.src = dataURL;
        };
    reader.readAsDataURL(input.files[0]);
    });

});